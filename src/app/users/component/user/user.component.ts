import { CreateUserComponent } from './../create-user/create-user.component';
import { Users } from './../../interface/user';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['id', 'nombre', 'username','acciones'];
  users: Users[];
  listObserver = []
  dataSource;
  newUser
  

  constructor(private userService: UserService,public dialog: MatDialog) { }
  
  ngOnInit(): void {
    this.getUsers()
  }
  getUsers(){
    const obs1$ = this.userService.getUsers().subscribe((resp:any) => {
      this.dataSource = resp
    })
    this.listObserver= [...this.listObserver,obs1$] 
  }
  delete(id:string){
    const obs2$ =  this.userService.deleteUser(id).subscribe((resp) => {
          console.log('usuario eliminado', resp)
          this.dataSource = resp
    })
    this.listObserver= [...this.listObserver, obs2$] 
  }
  createUser(){CreateUserComponent
    const dialogRef = this.dialog.open(CreateUserComponent,{width:'600px'})
    const obs3$ = dialogRef.afterClosed().subscribe( result => {
      console.log(result)
      if(result){
        const obs4$ = this.userService.createUser(result).subscribe((resp) => {
          this.dataSource = resp
        })
        this.listObserver= [...this.listObserver, obs4$] 
      }
      });
    this.listObserver= [...this.listObserver, obs3$] 
  }

  ngOnDestroy(): void{
      //me desuscribo de todo 
      this.listObserver.forEach(sub => sub.unsuscribe())
  }
}
