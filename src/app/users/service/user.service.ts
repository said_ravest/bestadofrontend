import { Users } from './../interface/user';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http: HttpClient) { }

  getUsers(){
    return this.http.get<Users>(`http://localhost:3000/api/user`)
  }
  deleteUser(id:string){
    return this.http.delete<Users>(`http://localhost:3000/api/user/${id}`)
  }
  createUser(user){
    return this.http.post<Users>(`http://localhost:3000/api/user/`,user)
  }
}
